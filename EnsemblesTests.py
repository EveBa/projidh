#On se concentre sur le set directOnly car le fichier est moins lourd à analyser.
#On choisit un GoTerm et on va utiliser les protéines qui l'annotennt comme query.
#On va ensuite regarder si le meilleur résultat obtenu suivant chaque mesure est bien le GOTerm dont on a extrait les protéines.
#GOTerm :GO:0031344   (34 prot annotées)

## Cas 1 : Q = T
#Q = l'ensemble des protéines qui annotent le GOTerm : (choisit arbitrairement car il possède ??? protéines annotées directement)
#
query_Q_eq_T = 'ZK757.2 ZK792.6 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 T07A5.6c M142.1c K09C8.5 K07G5.1 F59G1.5.1 F58A3.2c F55C7.7a F53C11.8.2 F45E10.1c F40E10.4 F36F2.5 F33D4.2a F28F9.1 F26C11.2 F15A2.6a F11A10.3a F10G8.6 F09B9.2b D1007.1 C52E12.2b C34F6.4 C14F5.5 C05D11.4 C01G10.11f.1 B0273.4a B0240.3 AC7.2a.2'
print(len(query_Q_eq_T))

#Cas 2 : Q complétement différent de T (pour ne pas biaiser les analyses autant de mots ont été récupérés que pour les query précédentes)
# Voir s'il y a une influence de la taille de l'ensemble
#
query_Q_diff_T='bonjour aurevoir toto tata tutu hello world tatu tuta titi titu tuti tarte flop tritre seize dsept dhuit dneuf vingt vingtun vingtdeux vingttrois vingtquatre vingtcinq vingtsix vingtsept vingthuit vingtneug trente trenteun trentedeux trentetrois trentequatre'
print(len(query_Q_diff_T))

#Cas 3: Q=T + d'autres termes qui n'ont rien à voir
#
#
query_Tinc_et_diff='bonjour aurevoir toto tata tutu hello world ZK757.2 ZK792.6 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 T07A5.6c M142.1c K09C8.5 K07G5.1 F59G1.5.1 F58A3.2c F55C7.7a F53C11.8.2 F45E10.1c F40E10.4 F36F2.5 F33D4.2a F28F9.1 F26C11.2 F15A2.6a F11A10.3a F10G8.6 F09B9.2b D1007.1 C52E12.2b C34F6.4 C14F5.5 C05D11.4 C01G10.11f.1 B0273.4a B0240.3 AC7.2a.2 bonjour aurevoir toto tata tutu hello world tatu tuta titi titu tuti tarte flop tritre seize dsept dhuit dneuf vingt vingtun vingtdeux vingttrois vingtquatre vingtcinq vingtsix vingtsept vingthuit vingtneug trente trenteun trentedeux trentetrois trentequatre er rt ty yu az ze lundi mardi mercredi jeudi samedi dimanche janvier fevrier mars avril mai juin juillet aout septembre octobre novembre decembre janjan febfev marmar apravr maymai junjui juljul augaou sepsep octoct novnov decdec ui io op pq qs sd df fg gh hj jk kl lm mw wx xc cv vb bn'
print(len(query_Tinc_et_diff))


#Cas 4: Q= T -1 ou 2 des prots +1 ou 2 mots au hasard (mm nb)
#
#
query_T_et_diff_bcp_commun='ZK757.2 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 M142.1c K09C8.5 K07G5.1 F59G1.5.1 F55C7.7a F53C11.8.2 F45E10.1c F40E10.4 F33D4.2a F28F9.1 F26C11.2 F15A2.6a F10G8.6 F09B9.2b D1007.1 C52E12.2b C34F6.4 C05D11.4 C01G10.11f.1 B0273.4a  AC7.2a.2'

#Cas 5: Q = mots qui ont rien à voir + 1 ou 2  prots du GOTerm (mm nb mots total que nb prot qui annotent GOTerm)
#Et on regarde les top des meilleurs résultats pour les différentes mesures
# 
#  
query_27diff_7T='ZK757.2 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 M142.1c tatu tuta titi titu tuti tarte flop tritre seize dsept dhuit dneuf vingt vingtun vingtdeux vingttrois vingtquatre vingtcinq vingtsix vingtsept vingthuit vingtneug trente trenteun trentedeux trentetrois trentequatre'

#Cas 6: Q = bcp de mots qui ont rien à voir + 1 ou 2  prots du GOTerm
#Et on regarde les top des meilleurs résultats pour les différentes mesures
# 
query_TsupT='ZK757.2 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 M142.1c bonjour aurevoir toto tata tutu hello world tatu tuta titi titu tuti tarte flop tritre seize dsept dhuit dneuf vingt vingtun vingtdeux vingttrois vingtquatre vingtcinq vingtsix vingtsept vingthuit vingtneug trente trenteun trentedeux trentetrois trentequatre er rt ty yu az ze lundi mardi mercredi jeudi samedi dimanche janvier fevrier mars avril mai juin juillet aout septembre octobre novembre decembre janjan febfev marmar apravr maymai junjui juljul augaou sepsep octoct novnov decdec ui io op pq qs sd df fg gh hj jk kl lm mw wx xc cv vb bn'
print(len(query_TsupT))

#Cas 7: Q = 1 ou 2 mots qui ont rien à voir + 1 ou 2  prots du GOTerm
#Et on regarde les top des meilleurs résultats pour les différentes mesures
# 
#  
query_7T_7diff='ZK757.2 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 M142.1c bonjour aurevoir toto tata tutu hello world'

#Cas 8: Q = 1 ou 2  prots du GOTerm
#Et on regarde les top des meilleurs résultats pour les différentes mesures
# 
#  
query_7T='ZK757.2 ZC84.2 Y60A3A.1.2 W02D3.9.2 T24B8.6 T08H4.1 M142.1c'