import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from scipy import meshgrid
from scipy.stats import binom, hypergeom, chi2_contingency
import numpy as np
import math


#Ici script pour comparer le comportement des p-values selon le type de mesure qui est effectué

def test(typeTest,q_size,t_size,c_size):
    g_size=q_size+t_size
    if typeTest=='binomial': # binom.cdf(>=success, attempts, proba)
            # p_success = 384/2064, 152 attempts, 61 success
            #~ pval = binom.pmf(61, 152, 384.0/2064)
            pval = binom.cdf( q_size - c_size, q_size, 1 - float(t_size)/(g_size))
    elif typeTest=='hypergeometric': # hypergeom.sf(common-1, population, target, query) = 1-p( X <= x-1 ) = p( X >= x )
            pval = hypergeom.sf(c_size-1, g_size, t_size, q_size)
    elif typeTest=='chi2':
            TabCont=np.array([[c_size,q_size-c_size],[t_size-c_size,g_size-q_size-t_size+c_size]])
            chi2, pval, dof, ex = chi2_contingency(TabCont, correction=False)
            #chi2 avec scipy
            #observed : array_like
            #The contingency table. The table contains the observed frequencies (i.e. number of occurrences) in each category. In the two-dimensional case, the table is often described as an “R x C table”.
    elif typeTest=='coverage':
            pval=1-((c_size/q_size)*(c_size/t_size))
    return(pval)

q_list=[]
t_list=[]
c_list=[]
pval_list=[]

for q in range(1,51):
    for t in range(1,51):
        for c in range(0,min(q,t)+1):
            pvalue=test('binomial',q,t,c)#changer selon le test qu'on veut faire: binomial, hypergeometric, chi2, coverage
            q_list.append(q)
            t_list.append(t)
            c_list.append(c)
            pval_list.append(pvalue)




fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_title('P-valeurs Binomiale selon tailles ensembles Q, T et C')
ax.set_xlabel('Taille Query')
ax.set_ylabel('Taille Target')
ax.set_zlabel('Taille éléments en Commun')
surf = ax.scatter(q_list, t_list, c_list, c=pval_list)

fig.colorbar(surf)


plt.show()
