GO:0010975	1.2281503657819794e-19	7/27	biological_process: regulation of neuron projection development	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0031344	9.300048081380361e-19	7/35	biological_process: regulation of cell projection organization	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0045664	1.423869430390655e-18	7/37	biological_process: regulation of neuron differentiation	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0050767	1.423869430390655e-18	7/37	biological_process: regulation of neurogenesis	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0051960	1.7453883340272433e-18	7/38	biological_process: regulation of nervous system development	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0060284	7.402492632046509e-18	7/46	biological_process: regulation of cell development	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0010769	2.941542591169004e-16	6/25	biological_process: regulation of cell morphogenesis involved in differentiation	W02D3.9.2, M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0050770	2.941542591169004e-16	6/25	biological_process: regulation of axonogenesis	W02D3.9.2, M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0045595	3.653765137482039e-16	7/78	biological_process: regulation of cell differentiation	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0022604	3.232645402459378e-15	6/36	biological_process: regulation of cell morphogenesis	W02D3.9.2, M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0022603	2.0348910600013026e-14	6/48	biological_process: regulation of anatomical structure morphogenesis	W02D3.9.2, M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0048699	1.4536326172365584e-12	7/248	biological_process: generation of neurons	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0022008	1.582910174505416e-12	7/251	biological_process: neurogenesis	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0051128	1.9234509748894082e-12	7/258	biological_process: regulation of cellular component organization	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:2000026	8.825670226489231e-12	7/320	biological_process: regulation of multicellular organismal development	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0032535	2.5824909996474254e-11	5/52	biological_process: regulation of cellular component size	M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0090066	4.5492831553551904e-11	5/58	biological_process: regulation of anatomical structure size	M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0030516	2.391854231575823e-10	4/21	biological_process: regulation of axon extension	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0061387	2.391854231575823e-10	4/21	biological_process: regulation of extent of cell growth	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0001558	7.005305244641735e-10	4/27	biological_process: regulation of cell growth	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0007399	7.587838102044057e-10	6/266	biological_process: nervous system development	W02D3.9.2, M142.1c, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0008361	1.6314161840077896e-09	4/33	biological_process: regulation of cell size	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0050793	2.002163732984997e-09	7/691	biological_process: regulation of developmental process	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0051239	3.0900078869481023e-09	7/735	biological_process: regulation of multicellular organismal process	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0065008	7.391575076040606e-07	6/838	biological_process: regulation of biological quality	W02D3.9.2, M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0030154	7.497239277093676e-07	6/840	biological_process: cell differentiation	W02D3.9.2, M142.1c, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0000902	9.233528324034026e-07	4/156	biological_process: cell morphogenesis	M142.1c, T08H4.1, Y60A3A.1.2, ZC84.2
GO:0048869	1.0436187439811934e-06	6/888	biological_process: cellular developmental process	W02D3.9.2, M142.1c, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0043005	2.118833081633985e-06	4/192	cellular_component: neuron projection	M142.1c, T08H4.1, Y60A3A.1.2, ZC84.2
GO:0010977	2.8990030401072713e-06	2/5	biological_process: negative regulation of neuron projection development	M142.1c, T08H4.1
GO:0045665	2.8990030401072713e-06	2/5	biological_process: negative regulation of neuron differentiation	M142.1c, T08H4.1
GO:0050768	2.8990030401072713e-06	2/5	biological_process: negative regulation of neurogenesis	M142.1c, T08H4.1
GO:0051961	4.347299554429889e-06	2/6	biological_process: negative regulation of nervous system development	M142.1c, T08H4.1
GO:0048731	5.529586392942004e-06	6/1176	biological_process: system development	W02D3.9.2, M142.1c, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0031345	6.084532788939173e-06	2/7	biological_process: negative regulation of cell projection organization	M142.1c, T08H4.1
GO:0097458	6.871981444751796e-06	4/258	cellular_component: neuron part	M142.1c, T08H4.1, Y60A3A.1.2, ZC84.2
GO:0042995	1.1228577681845366e-05	4/292	cellular_component: cell projection	M142.1c, T08H4.1, Y60A3A.1.2, ZC84.2
GO:0010721	1.3027447609012112e-05	2/10	biological_process: negative regulation of cell development	M142.1c, T08H4.1
GO:0032989	1.4781567935001927e-05	4/313	biological_process: cellular component morphogenesis	M142.1c, T08H4.1, Y60A3A.1.2, ZC84.2
GO:0045596	1.909633365051434e-05	2/12	biological_process: negative regulation of cell differentiation	M142.1c, T08H4.1
GO:0030424	2.7468444253553837e-05	3/113	cellular_component: axon	M142.1c, T08H4.1, Y60A3A.1.2
GO:0048638	3.6650420025023303e-05	4/394	biological_process: regulation of developmental growth	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0040008	4.3273962787848874e-05	4/411	biological_process: regulation of growth	M142.1c, T24B8.6, Y60A3A.1.2, ZC84.2
GO:0048858	5.673125551707079e-05	3/144	biological_process: cell projection morphogenesis	M142.1c, Y60A3A.1.2, ZC84.2
GO:0032990	6.156459595667865e-05	3/148	biological_process: cell part morphogenesis	M142.1c, Y60A3A.1.2, ZC84.2
GO:0007635	0.00011691899377960592	2/29	biological_process: chemosensory behavior	M142.1c, ZC84.2
GO:0016043	0.0001696077196884968	5/1195	biological_process: cellular component organization	Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0030182	0.00018167427181102697	3/213	biological_process: neuron differentiation	T24B8.6, Y60A3A.1.2, ZC84.2
GO:0071840	0.00020910300893100885	5/1248	biological_process: cellular component organization or biogenesis	Y60A3A.1.2, T08H4.1, ZC84.2, T24B8.6, ZK757.2
GO:0051129	0.0002128005096917756	2/39	biological_process: negative regulation of cellular component organization	M142.1c, T08H4.1
GO:0046662	0.00025903609321127125	2/43	biological_process: regulation of oviposition	M142.1c, T24B8.6
GO:0005515	0.0003183815389831699	5/1362	molecular_function: protein binding	W02D3.9.2, M142.1c, Y60A3A.1.2, T08H4.1, ZK757.2
GO:0048583	0.00039028401805914984	3/276	biological_process: regulation of response to stimulus	M142.1c, T08H4.1, T24B8.6
