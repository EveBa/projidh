GO:0010975	9.699611846147019e-16	7/27	biological_process: regulation of neuron projection development	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0031344	5.941718196352665e-15	7/35	biological_process: regulation of cell projection organization	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0045664	8.757993039495047e-15	7/37	biological_process: regulation of neuron differentiation	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0050767	8.757993039495047e-15	7/37	biological_process: regulation of neurogenesis	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0051960	1.055008783441435e-14	7/38	biological_process: regulation of nervous system development	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0060284	4.002231163691009e-14	7/46	biological_process: regulation of cell development	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0010769	2.3821879789508806e-13	6/25	biological_process: regulation of cell morphogenesis involved in differentiation	M142.1c, Y60A3A.1.2, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0050770	2.3821879789508806e-13	6/25	biological_process: regulation of axonogenesis	M142.1c, Y60A3A.1.2, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0045595	1.5869235170087134e-12	7/78	biological_process: regulation of cell differentiation	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0022604	2.110684551194899e-12	6/36	biological_process: regulation of cell morphogenesis	M142.1c, Y60A3A.1.2, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0022603	1.1778176240238811e-11	6/48	biological_process: regulation of anatomical structure morphogenesis	M142.1c, Y60A3A.1.2, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0032535	2.922100030475668e-09	5/52	biological_process: regulation of cellular component size	M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0048699	4.7760040909325525e-09	7/248	biological_process: generation of neurons	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0090066	5.025615735891729e-09	5/58	biological_process: regulation of anatomical structure size	M142.1c, Y60A3A.1.2, ZC84.2, T24B8.6, ZK757.2
GO:0022008	5.187339753905699e-09	7/251	biological_process: neurogenesis	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0051128	6.266033896445711e-09	7/258	biological_process: regulation of cellular component organization	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0030516	9.159955568275693e-09	4/21	biological_process: regulation of axon extension	Y60A3A.1.2, M142.1c, ZC84.2, T24B8.6
GO:0061387	9.159955568275693e-09	4/21	biological_process: regulation of extent of cell growth	Y60A3A.1.2, M142.1c, ZC84.2, T24B8.6
GO:0001558	2.4930842842239855e-08	4/27	biological_process: regulation of cell growth	Y60A3A.1.2, M142.1c, ZC84.2, T24B8.6
GO:2000026	2.739888043284669e-08	7/320	biological_process: regulation of multicellular organismal development	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0008361	5.54118882016337e-08	4/33	biological_process: regulation of cell size	Y60A3A.1.2, M142.1c, ZC84.2, T24B8.6
GO:0007399	3.009154074039512e-07	6/266	biological_process: nervous system development	M142.1c, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0050793	4.934018710164901e-06	7/691	biological_process: regulation of developmental process	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0051239	7.424443912344528e-06	7/735	biological_process: regulation of multicellular organismal process	M142.1c, Y60A3A.1.2, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0010977	1.5662534171458118e-05	2/5	biological_process: negative regulation of neuron projection development	T08H4.1, M142.1c
GO:0045665	1.5662534171458118e-05	2/5	biological_process: negative regulation of neuron differentiation	T08H4.1, M142.1c
GO:0050768	1.5662534171458118e-05	2/5	biological_process: negative regulation of neurogenesis	T08H4.1, M142.1c
GO:0051961	2.2539059866691024e-05	2/6	biological_process: negative regulation of nervous system development	T08H4.1, M142.1c
GO:0000902	2.549114923408709e-05	4/156	biological_process: cell morphogenesis	Y60A3A.1.2, T08H4.1, M142.1c, ZC84.2
GO:0031345	3.065777664718288e-05	2/7	biological_process: negative regulation of cell projection organization	T08H4.1, M142.1c
GO:0043005	5.7098682478006706e-05	4/192	cellular_component: neuron projection	Y60A3A.1.2, T08H4.1, M142.1c, ZC84.2
GO:0010721	6.244223664660326e-05	2/10	biological_process: negative regulation of cell development	T08H4.1, M142.1c
GO:0045596	8.979735748967509e-05	2/12	biological_process: negative regulation of cell differentiation	T08H4.1, M142.1c
GO:0097458	0.0001781007477841662	4/258	cellular_component: neuron part	Y60A3A.1.2, T08H4.1, M142.1c, ZC84.2
GO:0065008	0.00020997929793806924	6/838	biological_process: regulation of biological quality	M142.1c, Y60A3A.1.2, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0030154	0.00021274872772687796	6/840	biological_process: cell differentiation	M142.1c, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
GO:0030424	0.00027901988977451956	3/113	cellular_component: axon	Y60A3A.1.2, T08H4.1, M142.1c
GO:0042995	0.00028562085569101695	4/292	cellular_component: cell projection	Y60A3A.1.2, T08H4.1, M142.1c, ZC84.2
GO:0048869	0.00028849820079479994	6/888	biological_process: cellular developmental process	M142.1c, T08H4.1, ZC84.2, W02D3.9.2, T24B8.6, ZK757.2
