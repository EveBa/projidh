GO:0031344	3.31677468291017e-69	27/35	biological_process: regulation of cell projection organization	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, AC7.2a.2, K09C8.5, T24B8.6, F55C7.7a, F33D4.2a, F10G8.6, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0010975	4.216379800885472e-54	22/27	biological_process: regulation of neuron projection development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0045664	4.301740342678976e-51	22/37	biological_process: regulation of neuron differentiation	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0050767	4.301740342678976e-51	22/37	biological_process: regulation of neurogenesis	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0051960	7.731651108418196e-51	22/38	biological_process: regulation of nervous system development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0060284	5.156081862741799e-49	22/46	biological_process: regulation of cell development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0010769	1.969924773993612e-48	20/25	biological_process: regulation of cell morphogenesis involved in differentiation	C01G10.11f.1, C52E12.2b, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0050770	1.969924773993612e-48	20/25	biological_process: regulation of axonogenesis	C01G10.11f.1, C52E12.2b, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0022604	2.8781402342769944e-48	21/36	biological_process: regulation of cell morphogenesis	C01G10.11f.1, C52E12.2b, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, T24B8.6, F55C7.7a, F10G8.6, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0051128	8.802597280864276e-46	27/258	biological_process: regulation of cellular component organization	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, AC7.2a.2, K09C8.5, T24B8.6, F55C7.7a, F33D4.2a, F10G8.6, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0022603	1.203196346675153e-45	21/48	biological_process: regulation of anatomical structure morphogenesis	C01G10.11f.1, C52E12.2b, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, T24B8.6, F55C7.7a, F10G8.6, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0045595	5.651679336058007e-44	22/78	biological_process: regulation of cell differentiation	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0030516	9.494177076595276e-38	16/21	biological_process: regulation of axon extension	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, B0273.4a, D1007.1, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0061387	9.494177076595276e-38	16/21	biological_process: regulation of extent of cell growth	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, B0273.4a, D1007.1, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0001558	5.26650662226462e-36	16/27	biological_process: regulation of cell growth	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0048699	2.717940749431556e-35	23/248	biological_process: generation of neurons	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0022008	3.580333299275941e-35	23/251	biological_process: neurogenesis	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0008361	1.2992066137265104e-34	16/33	biological_process: regulation of cell size	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0032535	5.184456058214205e-34	17/52	biological_process: regulation of cellular component size	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, ZK757.2, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0090066	3.302693312120587e-33	17/58	biological_process: regulation of anatomical structure size	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, ZK757.2, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:2000026	9.334891832082033e-33	23/320	biological_process: regulation of multicellular organismal development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0007399	4.4787334818616304e-30	21/266	biological_process: nervous system development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2
GO:0050793	4.0889372076119514e-27	24/691	biological_process: regulation of developmental process	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, F10G8.6, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0051239	1.6456509358070062e-24	23/735	biological_process: regulation of multicellular organismal process	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0030154	3.4249776626262195e-23	23/840	biological_process: cell differentiation	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, F33D4.2a, ZK757.2, K07G5.1, ZC84.2
GO:0048869	1.2094921267404391e-22	23/888	biological_process: cellular developmental process	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, F33D4.2a, ZK757.2, K07G5.1, ZC84.2
GO:0030182	1.0084017748099862e-21	16/213	biological_process: neuron differentiation	F45E10.1c, C01G10.11f.1, F26C11.2, C34F6.4, D1007.1, B0273.4a, T24B8.6, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0048666	2.5354573261164215e-20	15/198	biological_process: neuron development	F45E10.1c, C01G10.11f.1, F26C11.2, C34F6.4, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0000902	6.495229598306503e-20	14/156	biological_process: cell morphogenesis	F45E10.1c, M142.1c, D1007.1, B0273.4a, F09B9.2b, T08H4.1, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0048731	6.999638619406599e-20	23/1176	biological_process: system development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, AC7.2a.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2
GO:0065008	9.84333182840459e-20	21/838	biological_process: regulation of biological quality	C01G10.11f.1, C52E12.2b, W02D3.9.2, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, T24B8.6, F55C7.7a, F33D4.2a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0048638	3.5275406311882907e-19	17/394	biological_process: regulation of developmental growth	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, C34F6.4, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0031175	5.904618819918701e-19	14/183	biological_process: neuron projection development	F45E10.1c, C01G10.11f.1, C34F6.4, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0040008	7.134583053881477e-19	17/411	biological_process: regulation of growth	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, C34F6.4, D1007.1, B0273.4a, C52E12.2b, T24B8.6, F55C7.7a, F40E10.4, F53C11.8.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0048858	1.77293384186739e-18	13/144	biological_process: cell projection morphogenesis	F45E10.1c, M142.1c, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0032990	2.5204934658225642e-18	13/148	biological_process: cell part morphogenesis	F45E10.1c, M142.1c, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0048812	2.681173048387891e-18	12/104	biological_process: neuron projection morphogenesis	F45E10.1c, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0030030	7.970326592116775e-18	14/221	biological_process: cell projection organization	F45E10.1c, C01G10.11f.1, C34F6.4, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0048468	1.4477902014622354e-16	18/695	biological_process: cell development	F45E10.1c, C01G10.11f.1, F26C11.2, C34F6.4, D1007.1, B0273.4a, F09B9.2b, F55C7.7a, F40E10.4, F33D4.2a, ZC84.2, W02D3.9.2, F15A2.6a, K07G5.1, F28F9.1, F59G1.5.1, K09C8.5, Y60A3A.1.2
GO:0032989	9.473475558359892e-16	14/313	biological_process: cellular component morphogenesis	F45E10.1c, M142.1c, D1007.1, B0273.4a, F09B9.2b, T08H4.1, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0006935	1.09319349125023e-15	11/120	biological_process: chemotaxis	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0007411	2.082470697629948e-15	10/84	biological_process: axon guidance	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0097485	2.082470697629948e-15	10/84	biological_process: neuron projection guidance	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0042330	2.2016172540981334e-15	11/128	biological_process: taxis	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0016043	3.865262741966489e-15	20/1195	biological_process: cellular component organization	C01G10.11f.1, C52E12.2b, T08H4.1, F45E10.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0007409	7.028293007671703e-15	10/95	biological_process: axonogenesis	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0071840	8.912864072807829e-15	20/1248	biological_process: cellular component organization or biogenesis	C01G10.11f.1, C52E12.2b, T08H4.1, F45E10.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C05D11.4, C34F6.4, D1007.1, F40E10.4, F53C11.8.2, K09C8.5, T24B8.6, F55C7.7a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0048667	1.0561368591588585e-14	10/99	biological_process: cell morphogenesis involved in neuron differentiation	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0031345	1.1358449677122669e-14	6/7	biological_process: negative regulation of cell projection organization	C05D11.4, M142.1c, T08H4.1, F40E10.4, F10G8.6, K09C8.5
GO:0000904	1.1662905956279866e-14	10/100	biological_process: cell morphogenesis involved in differentiation	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0061564	2.0082865710317458e-14	11/157	biological_process: axon development	F45E10.1c, C01G10.11f.1, D1007.1, B0273.4a, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0040012	2.4640177742250856e-14	11/160	biological_process: regulation of locomotion	F45E10.1c, C05D11.4, C34F6.4, B0273.4a, T24B8.6, T08H4.1, F55C7.7a, F40E10.4, F53C11.8.2, F33D4.2a, K07G5.1
GO:0051270	1.4698531221028203e-12	8/65	biological_process: regulation of cellular component movement	F45E10.1c, C05D11.4, C34F6.4, T24B8.6, F40E10.4, F53C11.8.2, F33D4.2a, K07G5.1
GO:0051129	3.1539582813678385e-12	7/39	biological_process: negative regulation of cellular component organization	C05D11.4, M142.1c, T08H4.1, F40E10.4, F53C11.8.2, F10G8.6, K09C8.5
GO:0051962	5.323575309745908e-12	5/7	biological_process: positive regulation of nervous system development	F45E10.1c, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1
GO:1902667	5.323575309745908e-12	5/7	biological_process: regulation of axon guidance	C05D11.4, C34F6.4, T24B8.6, F40E10.4, K07G5.1
GO:0042221	1.271198424657251e-11	14/632	biological_process: response to chemical	F45E10.1c, C05D11.4, M142.1c, D1007.1, B0273.4a, F55C7.7a, F40E10.4, AC7.2a.2, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0040011	1.340326838255145e-11	19/1579	biological_process: locomotion	C52E12.2b, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, F26C11.2, C34F6.4, D1007.1, F09B9.2b, F40E10.4, K09C8.5, F55C7.7a, F33D4.2a, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0009605	1.351549946074213e-11	11/288	biological_process: response to external stimulus	F45E10.1c, B0273.4a, D1007.1, F55C7.7a, F40E10.4, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0032101	5.5566691401163925e-11	6/29	biological_process: regulation of response to external stimulus	C05D11.4, C34F6.4, T24B8.6, F40E10.4, K07G5.1, K09C8.5
GO:0009653	6.826189645045874e-11	16/1062	biological_process: anatomical structure morphogenesis	F45E10.1c, M142.1c, D1007.1, B0273.4a, C52E12.2b, F09B9.2b, T08H4.1, F55C7.7a, F40E10.4, F33D4.2a, ZC84.2, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0030424	1.1459998032091106e-10	8/113	cellular_component: axon	C01G10.11f.1, F26C11.2, M142.1c, C52E12.2b, T08H4.1, F55C7.7a, F15A2.6a, Y60A3A.1.2
GO:0044087	1.4323437962206413e-10	6/34	biological_process: regulation of cellular component biogenesis	C52E12.2b, F09B9.2b, F55C7.7a, F33D4.2a, F10G8.6, ZK757.2
GO:0006928	1.43836825205308e-10	11/360	biological_process: movement of cell or subcellular component	F45E10.1c, C34F6.4, D1007.1, B0273.4a, F55C7.7a, F40E10.4, F15A2.6a, K07G5.1, F28F9.1, K09C8.5, Y60A3A.1.2
GO:0048583	2.3795839715270803e-10	10/276	biological_process: regulation of response to stimulus	C05D11.4, M142.1c, C34F6.4, T24B8.6, T08H4.1, F40E10.4, F33D4.2a, K07G5.1, F59G1.5.1, K09C8.5
GO:0043005	2.4244308395830595e-10	9/192	cellular_component: neuron projection	C01G10.11f.1, F26C11.2, M142.1c, C52E12.2b, T08H4.1, F55C7.7a, F15A2.6a, ZC84.2, Y60A3A.1.2
GO:0010770	5.193798108654051e-10	4/5	biological_process: positive regulation of cell morphogenesis involved in differentiation	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0010976	5.193798108654051e-10	4/5	biological_process: positive regulation of neuron projection development	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0031346	5.193798108654051e-10	4/5	biological_process: positive regulation of cell projection organization	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0045773	5.193798108654051e-10	4/5	biological_process: positive regulation of axon extension	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0050772	5.193798108654051e-10	4/5	biological_process: positive regulation of axonogenesis	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0060491	5.193798108654051e-10	4/5	biological_process: regulation of cell projection assembly	F10G8.6, F09B9.2b, F33D4.2a, ZK757.2
GO:0010977	5.193798108654051e-10	4/5	biological_process: negative regulation of neuron projection development	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0045665	5.193798108654051e-10	4/5	biological_process: negative regulation of neuron differentiation	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0050768	5.193798108654051e-10	4/5	biological_process: negative regulation of neurogenesis	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0050920	5.88564508133208e-10	5/18	biological_process: regulation of chemotaxis	C05D11.4, C34F6.4, T24B8.6, F40E10.4, K07G5.1
GO:0032879	6.245912218352477e-10	9/214	biological_process: regulation of localization	F45E10.1c, C05D11.4, C34F6.4, T24B8.6, F40E10.4, F53C11.8.2, F33D4.2a, K07G5.1, Y60A3A.1.2
GO:0030307	1.0753401986952698e-09	4/6	biological_process: positive regulation of cell growth	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0045666	1.0753401986952698e-09	4/6	biological_process: positive regulation of neuron differentiation	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0050769	1.0753401986952698e-09	4/6	biological_process: positive regulation of neurogenesis	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0051961	1.0753401986952698e-09	4/6	biological_process: negative regulation of nervous system development	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0048841	1.0753401986952698e-09	4/6	biological_process: regulation of axon extension involved in axon guidance	C05D11.4, F40E10.4, K07G5.1, T24B8.6
GO:0050795	1.5891852611961585e-09	7/96	biological_process: regulation of behavior	C05D11.4, M142.1c, C34F6.4, T24B8.6, F40E10.4, F33D4.2a, K07G5.1
GO:0005515	2.75224445493465e-09	16/1362	molecular_function: protein binding	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, F09B9.2b, T08H4.1, F55C7.7a, F40E10.4, F53C11.8.2, F33D4.2a, W02D3.9.2, F15A2.6a, ZK757.2, K07G5.1, Y60A3A.1.2
GO:0097458	3.1647723746389534e-09	9/258	cellular_component: neuron part	C01G10.11f.1, F26C11.2, M142.1c, C52E12.2b, T08H4.1, F55C7.7a, F15A2.6a, ZC84.2, Y60A3A.1.2
GO:0048523	3.3751517291285627e-09	10/364	biological_process: negative regulation of cellular process	C05D11.4, M142.1c, T08H4.1, F40E10.4, F53C11.8.2, F10G8.6, W02D3.9.2, K07G5.1, F28F9.1, K09C8.5
GO:0010720	3.3882268738982184e-09	4/8	biological_process: positive regulation of cell development	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0048856	5.3890890558346985e-09	24/4101	biological_process: anatomical structure development	C01G10.11f.1, C52E12.2b, T08H4.1, W02D3.9.2, F59G1.5.1, F45E10.1c, M142.1c, B0273.4a, F15A2.6a, F28F9.1, C05D11.4, C34F6.4, D1007.1, F09B9.2b, F40E10.4, F53C11.8.2, AC7.2a.2, K09C8.5, T24B8.6, F55C7.7a, F33D4.2a, ZK757.2, K07G5.1, ZC84.2
GO:0045597	8.246775577097447e-09	4/10	biological_process: positive regulation of cell differentiation	F45E10.1c, F55C7.7a, F28F9.1, B0273.4a
GO:0010721	8.246775577097447e-09	4/10	biological_process: negative regulation of cell development	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0042995	9.204257620346572e-09	9/292	cellular_component: cell projection	C01G10.11f.1, F26C11.2, M142.1c, C52E12.2b, T08H4.1, F55C7.7a, F15A2.6a, ZC84.2, Y60A3A.1.2
GO:0045596	1.7048288563863024e-08	4/12	biological_process: negative regulation of cell differentiation	T08H4.1, F40E10.4, M142.1c, K09C8.5
GO:0050794	2.041701294667521e-08	18/2097	biological_process: regulation of cellular process	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, C34F6.4, B0273.4a, T24B8.6, C52E12.2b, F09B9.2b, F40E10.4, F53C11.8.2, F10G8.6, F15A2.6a, ZK757.2, K07G5.1, F59G1.5.1, K09C8.5, Y60A3A.1.2
GO:0051130	2.3938919761485575e-08	5/38	biological_process: positive regulation of cellular component organization	F45E10.1c, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1
GO:0048522	3.064530444920759e-08	9/336	biological_process: positive regulation of cellular process	F45E10.1c, B0273.4a, T24B8.6, C52E12.2b, T08H4.1, F55C7.7a, F28F9.1, F59G1.5.1, ZC84.2
GO:0050803	5.355240116885997e-08	4/16	biological_process: regulation of synapse structure or activity	C52E12.2b, C01G10.11f.1, F26C11.2, W02D3.9.2
GO:0030334	1.9234212899361206e-07	5/58	biological_process: regulation of cell migration	F45E10.1c, C34F6.4, F33D4.2a, F53C11.8.2, K07G5.1
GO:2000145	2.463741845137325e-07	5/61	biological_process: regulation of cell motility	F45E10.1c, C34F6.4, F33D4.2a, F53C11.8.2, K07G5.1
GO:0050789	5.580942039067905e-07	18/2579	biological_process: regulation of biological process	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, C34F6.4, B0273.4a, T24B8.6, C52E12.2b, F09B9.2b, F40E10.4, F53C11.8.2, F10G8.6, F15A2.6a, ZK757.2, K07G5.1, F59G1.5.1, K09C8.5, Y60A3A.1.2
GO:0007517	5.665732893718238e-07	4/29	biological_process: muscle organ development	F45E10.1c, K09C8.5, F59G1.5.1, AC7.2a.2
GO:0048519	1.1027871708090745e-06	10/677	biological_process: negative regulation of biological process	C05D11.4, M142.1c, T08H4.1, F40E10.4, F53C11.8.2, F10G8.6, W02D3.9.2, K07G5.1, F28F9.1, K09C8.5
GO:0008078	2.198593655244493e-06	3/11	biological_process: mesodermal cell migration	F45E10.1c, F55C7.7a, B0273.4a
GO:0016358	2.934342591582064e-06	4/44	biological_process: dendrite development	F09B9.2b, F55C7.7a, C34F6.4, B0273.4a
GO:0050896	5.267048509658542e-06	14/1737	biological_process: response to stimulus	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, D1007.1, B0273.4a, T08H4.1, F40E10.4, AC7.2a.2, F15A2.6a, K07G5.1, F28F9.1, ZC84.2, Y60A3A.1.2
GO:0065007	6.174201348879873e-06	18/3012	biological_process: biological regulation	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, C34F6.4, B0273.4a, T24B8.6, C52E12.2b, F09B9.2b, F40E10.4, F53C11.8.2, F10G8.6, F15A2.6a, ZK757.2, K07G5.1, F59G1.5.1, K09C8.5, Y60A3A.1.2
GO:0036477	1.644551031720676e-05	5/145	cellular_component: somatodendritic compartment	C01G10.11f.1, C52E12.2b, F15A2.6a, ZC84.2, Y60A3A.1.2
GO:0010591	2.1730490632837127e-05	2/3	biological_process: regulation of lamellipodium assembly	F09B9.2b, ZK757.2
GO:0051489	2.1730490632837127e-05	2/3	biological_process: regulation of filopodium assembly	F09B9.2b, F33D4.2a
GO:1902743	2.1730490632837127e-05	2/3	biological_process: regulation of lamellipodium organization	F09B9.2b, ZK757.2
GO:0010771	2.1730490632837127e-05	2/3	biological_process: negative regulation of cell morphogenesis involved in differentiation	F40E10.4, M142.1c
GO:0030517	2.1730490632837127e-05	2/3	biological_process: negative regulation of axon extension	F40E10.4, M142.1c
GO:0050771	2.1730490632837127e-05	2/3	biological_process: negative regulation of axonogenesis	F40E10.4, M142.1c
GO:0035020	2.1730490632837127e-05	2/3	biological_process: regulation of Rac protein signal transduction	F55C7.7a, K07G5.1
GO:0001667	2.2395430347230783e-05	3/24	biological_process: ameboidal-type cell migration	F45E10.1c, F55C7.7a, B0273.4a
GO:0016477	2.733838902255224e-05	6/274	biological_process: cell migration	F45E10.1c, C34F6.4, B0273.4a, F55C7.7a, F40E10.4, Y60A3A.1.2
GO:0051094	2.8457997571856862e-05	7/416	biological_process: positive regulation of developmental process	F45E10.1c, C34F6.4, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1, F59G1.5.1
GO:0048870	3.085149746238551e-05	6/280	biological_process: cell motility	F45E10.1c, C34F6.4, B0273.4a, F55C7.7a, F40E10.4, Y60A3A.1.2
GO:0051674	3.085149746238551e-05	6/280	biological_process: localization of cell	F45E10.1c, C34F6.4, B0273.4a, F55C7.7a, F40E10.4, Y60A3A.1.2
GO:0051240	3.165335010683545e-05	7/423	biological_process: positive regulation of multicellular organismal process	F45E10.1c, C34F6.4, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1, F59G1.5.1
GO:0016203	3.8578528318078836e-05	2/4	biological_process: muscle attachment	F45E10.1c, K09C8.5
GO:0060538	3.8578528318078836e-05	2/4	biological_process: skeletal muscle organ development	F45E10.1c, K09C8.5
GO:0032102	3.8578528318078836e-05	2/4	biological_process: negative regulation of response to external stimulus	F40E10.4, K09C8.5
GO:0007610	5.242077346279715e-05	7/458	biological_process: behavior	F45E10.1c, M142.1c, C34F6.4, C52E12.2b, F55C7.7a, F33D4.2a, ZC84.2
GO:0008543	6.019555185588889e-05	2/5	biological_process: fibroblast growth factor receptor signaling pathway	C05D11.4, AC7.2a.2
GO:0044344	6.019555185588889e-05	2/5	biological_process: cellular response to fibroblast growth factor stimulus	C05D11.4, AC7.2a.2
GO:0071774	6.019555185588889e-05	2/5	biological_process: response to fibroblast growth factor	C05D11.4, AC7.2a.2
GO:0061061	6.0785657720141644e-05	5/191	biological_process: muscle structure development	F45E10.1c, AC7.2a.2, K07G5.1, F59G1.5.1, K09C8.5
GO:0008088	8.656168012693398e-05	2/6	biological_process: axo-dendritic transport	C52E12.2b, F15A2.6a
GO:0030308	8.656168012693398e-05	2/6	biological_process: negative regulation of cell growth	F40E10.4, M142.1c
GO:0008092	0.00011101858822653181	4/112	molecular_function: cytoskeletal protein binding	F45E10.1c, C01G10.11f.1, F33D4.2a, ZK757.2
GO:0051271	0.00011765709153990573	2/7	biological_process: negative regulation of cellular component movement	F40E10.4, F53C11.8.2
GO:0048639	0.00012159499617748825	6/359	biological_process: positive regulation of developmental growth	F45E10.1c, C34F6.4, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1
GO:0051093	0.0001509371868250361	5/232	biological_process: negative regulation of developmental process	M142.1c, T08H4.1, F40E10.4, F10G8.6, K09C8.5
GO:0045927	0.0001541667391092227	6/375	biological_process: positive regulation of growth	F45E10.1c, C34F6.4, B0273.4a, C52E12.2b, F55C7.7a, F28F9.1
GO:0046578	0.00016251525959408338	3/47	biological_process: regulation of Ras protein signal transduction	T08H4.1, F59G1.5.1, K07G5.1
GO:0007416	0.00019395677415529872	2/9	biological_process: synapse assembly	F26C11.2, F15A2.6a
GO:0046579	0.00019395677415529872	2/9	biological_process: positive regulation of Ras protein signal transduction	F55C7.7a, F59G1.5.1
GO:0051057	0.00019395677415529872	2/9	biological_process: positive regulation of small GTPase mediated signal transduction	F55C7.7a, F59G1.5.1
GO:0007154	0.0001974716474881058	10/1226	biological_process: cell communication	F45E10.1c, C01G10.11f.1, C05D11.4, B0273.4a, C52E12.2b, T08H4.1, AC7.2a.2, W02D3.9.2, ZC84.2, Y60A3A.1.2
GO:0030425	0.0002573357770706195	3/55	cellular_component: dendrite	C52E12.2b, F15A2.6a, ZC84.2
GO:0051056	0.0002573357770706195	3/55	biological_process: regulation of small GTPase mediated signal transduction	T08H4.1, F59G1.5.1, K07G5.1
GO:0030163	0.00027900487631336076	5/265	biological_process: protein catabolic process	M142.1c, B0273.4a, F55C7.7a, F33D4.2a, Y60A3A.1.2
GO:0050807	0.0002889372117711845	2/11	biological_process: regulation of synapse organization	C52E12.2b, C01G10.11f.1
GO:0048640	0.0002889372117711845	2/11	biological_process: negative regulation of developmental growth	F40E10.4, M142.1c
GO:0044089	0.0002889372117711845	2/11	biological_process: positive regulation of cellular component biogenesis	C52E12.2b, F55C7.7a
GO:0017048	0.0003433837878962337	2/12	molecular_function: Rho GTPase binding	T08H4.1, F55C7.7a
GO:0048518	0.00034824737150337845	8/831	biological_process: positive regulation of biological process	F45E10.1c, C34F6.4, B0273.4a, T24B8.6, C52E12.2b, F28F9.1, F59G1.5.1, ZC84.2
GO:0045926	0.0004024419592271123	2/13	biological_process: negative regulation of growth	F40E10.4, M142.1c
GO:0007265	0.00046609231663796697	2/14	biological_process: Ras protein signal transduction	T08H4.1, F55C7.7a
GO:0044464	0.0006469964639182204	16/3363	cellular_component: cell part	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, B0273.4a, T24B8.6, F09B9.2b, T08H4.1, F55C7.7a, F53C11.8.2, AC7.2a.2, F15A2.6a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0005623	0.000656406721982557	16/3367	cellular_component: cell	F45E10.1c, C01G10.11f.1, C05D11.4, M142.1c, B0273.4a, T24B8.6, F09B9.2b, T08H4.1, F55C7.7a, F53C11.8.2, AC7.2a.2, F15A2.6a, ZK757.2, K07G5.1, ZC84.2, Y60A3A.1.2
GO:0090598	0.0007357616196200777	3/79	biological_process: male anatomical structure morphogenesis	F55C7.7a, B0273.4a, Y60A3A.1.2
GO:0070848	0.0007662294627669248	2/18	biological_process: response to growth factor	C05D11.4, AC7.2a.2
GO:0071363	0.0007662294627669248	2/18	biological_process: cellular response to growth factor stimulus	C05D11.4, AC7.2a.2
GO:0050808	0.0007662294627669248	2/18	biological_process: synapse organization	F26C11.2, F15A2.6a
GO:0023052	0.0008402907222728786	9/1197	biological_process: signaling	F45E10.1c, C01G10.11f.1, C05D11.4, B0273.4a, C52E12.2b, T08H4.1, AC7.2a.2, W02D3.9.2, ZC84.2
GO:0051015	0.0008525516498102915	2/19	molecular_function: actin filament binding	F45E10.1c, F09B9.2b
GO:0008038	0.0009082925737667662	3/85	biological_process: neuron recognition	C01G10.11f.1, F26C11.2, F55C7.7a
GO:0033563	0.0009433507877716641	2/20	biological_process: dorsal/ventral axon guidance	F40E10.4, B0273.4a
GO:0008064	0.0009433507877716641	2/20	biological_process: regulation of actin polymerization or depolymerization	F55C7.7a, ZK757.2
GO:0030832	0.0009433507877716641	2/20	biological_process: regulation of actin filament length	F55C7.7a, ZK757.2
GO:0008037	0.0009710270295659997	3/87	biological_process: cell recognition	C01G10.11f.1, F26C11.2, F55C7.7a
GO:0046661	0.0009710270295659997	3/87	biological_process: male sex differentiation	F55C7.7a, B0273.4a, Y60A3A.1.2
GO:0048585	0.0010033995514509785	3/88	biological_process: negative regulation of response to stimulus	K07G5.1, F40E10.4, K09C8.5
GO:0010970	0.0010386078726736599	2/21	biological_process: transport along microtubule	C52E12.2b, F15A2.6a
GO:0009057	0.0010483742712922338	5/355	biological_process: macromolecule catabolic process	M142.1c, B0273.4a, F55C7.7a, F33D4.2a, Y60A3A.1.2
GO:0040013	0.001138303957826153	2/22	biological_process: negative regulation of locomotion	F40E10.4, F53C11.8.2
GO:1902531	0.0011755381574761745	3/93	biological_process: regulation of intracellular signal transduction	T08H4.1, F59G1.5.1, K07G5.1
GO:0030705	0.0012424201536806706	2/23	biological_process: cytoskeleton-dependent intracellular transport	C52E12.2b, F15A2.6a
