GO:0009408	7.653127884524355e-07	4/53	biological_process: response to heat	R13H8.1h, F38A6.3b, T19E7.2a.2, B0261.2a.2
GO:0048519	1.4893632056935268e-06	8/677	biological_process: negative regulation of biological process	R13H8.1h, F55A8.2a.1, F43C1.2b, K10B2.1, H39E23.1d, T19A5.2a, W01G7.1, F38A6.3b
GO:0050793	1.7387898569018102e-06	8/691	biological_process: regulation of developmental process	R12B2.5a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, H39E23.1d, Y106G6E.6.2, T19A5.2a, T19E7.2a.2
GO:0009266	2.2283034350079098e-06	4/69	biological_process: response to temperature stimulus	R13H8.1h, F38A6.3b, T19E7.2a.2, B0261.2a.2
GO:0051239	2.7697815830550617e-06	8/735	biological_process: regulation of multicellular organismal process	R12B2.5a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, H39E23.1d, Y106G6E.6.2, T19A5.2a, T19E7.2a.2
GO:0005515	3.073098823658922e-06	10/1362	molecular_function: protein binding	R12B2.5a.1, ZK742.1a.1, R13H8.1h, F43C1.2b, K10B2.1, H39E23.1d, F38A6.3b, T19A5.2a, W01G7.1, T19E7.2a.2
GO:0008340	6.230932381900071e-06	8/819	biological_process: determination of adult lifespan	R12B2.5a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, F38A6.3b, K10B2.1, T19E7.2a.2, B0261.2a.2
GO:0010259	6.230932381900071e-06	8/819	biological_process: multicellular organism aging	R12B2.5a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, F38A6.3b, K10B2.1, T19E7.2a.2, B0261.2a.2
GO:0007568	6.287898691692787e-06	8/820	biological_process: aging	R12B2.5a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, F38A6.3b, K10B2.1, T19E7.2a.2, B0261.2a.2
GO:0034605	1.1255331743858473e-05	2/4	biological_process: cellular response to heat	R13H8.1h, T19E7.2a.2
GO:0043620	1.1255331743858473e-05	2/4	biological_process: regulation of DNA-templated transcription in response to stress	R13H8.1h, T19E7.2a.2
GO:0002119	1.33006844246375e-05	11/2019	biological_process: nematode larval development	R12B2.5a.1, ZK742.1a.1, R13H8.1h, F43C1.2b, H39E23.1d, Y106G6E.6.2, C07F11.1, K10B2.1, K04G7.3a, T19E7.2a.2, B0261.2a.2
GO:0002164	1.3432286346683248e-05	11/2021	biological_process: larval development	R12B2.5a.1, ZK742.1a.1, R13H8.1h, F43C1.2b, H39E23.1d, Y106G6E.6.2, C07F11.1, K10B2.1, K04G7.3a, T19E7.2a.2, B0261.2a.2
GO:0009791	1.4598339334432433e-05	11/2038	biological_process: post-embryonic development	R12B2.5a.1, ZK742.1a.1, R13H8.1h, F43C1.2b, H39E23.1d, Y106G6E.6.2, C07F11.1, K10B2.1, K04G7.3a, T19E7.2a.2, B0261.2a.2
GO:1901700	2.0749063258543115e-05	3/39	biological_process: response to oxygen-containing compound	R13H8.1h, F55A8.2a.1, T19E7.2a.2
GO:0010557	2.2329648308035293e-05	4/123	biological_process: positive regulation of macromolecule biosynthetic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0005634	2.412967613821007e-05	9/1321	cellular_component: nucleus	R12B2.5a.1, ZK742.1a.1, R13H8.1h, F55A8.2a.1, F43C1.2b, K10B2.1, K04G7.3a, F38A6.3b, B0261.2a.2
GO:0009891	2.4554225554956706e-05	4/126	biological_process: positive regulation of biosynthetic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0031328	2.4554225554956706e-05	4/126	biological_process: positive regulation of cellular biosynthetic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0010628	2.5330728514871096e-05	4/127	biological_process: positive regulation of gene expression	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0051173	2.5330728514871096e-05	4/127	biological_process: positive regulation of nitrogen compound metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0042981	3.655794497530315e-05	3/47	biological_process: regulation of apoptotic process	R13H8.1h, T19A5.2a, F38A6.3b
GO:0040015	6.725182182970247e-05	2/9	biological_process: negative regulation of multicellular organism growth	F55A8.2a.1, R13H8.1h
GO:0009628	6.895255254913264e-05	4/164	biological_process: response to abiotic stimulus	R13H8.1h, F38A6.3b, T19E7.2a.2, B0261.2a.2
GO:0010604	7.228467448136957e-05	4/166	biological_process: positive regulation of macromolecule metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0043067	7.628033333776552e-05	3/60	biological_process: regulation of programmed cell death	R13H8.1h, T19A5.2a, F38A6.3b
GO:0048522	7.734464594863686e-05	5/336	biological_process: positive regulation of cellular process	R12B2.5a.1, R13H8.1h, F43C1.2b, T19E7.2a.2, B0261.2a.2
GO:0031325	7.930005006032862e-05	4/170	biological_process: positive regulation of cellular metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0008286	8.399491844608697e-05	2/10	biological_process: insulin receptor signaling pathway	F55A8.2a.1, R13H8.1h
GO:0032868	8.399491844608697e-05	2/10	biological_process: response to insulin	F55A8.2a.1, R13H8.1h
GO:0032869	8.399491844608697e-05	2/10	biological_process: cellular response to insulin stimulus	F55A8.2a.1, R13H8.1h
GO:0043434	8.399491844608697e-05	2/10	biological_process: response to peptide hormone	F55A8.2a.1, R13H8.1h
GO:0071375	8.399491844608697e-05	2/10	biological_process: cellular response to peptide hormone stimulus	F55A8.2a.1, R13H8.1h
GO:1901652	8.399491844608697e-05	2/10	biological_process: response to peptide	F55A8.2a.1, R13H8.1h
GO:1901653	8.399491844608697e-05	2/10	biological_process: cellular response to peptide	F55A8.2a.1, R13H8.1h
GO:0048640	0.00010257514738405587	2/11	biological_process: negative regulation of developmental growth	F55A8.2a.1, R13H8.1h
GO:0071417	0.00010257514738405587	2/11	biological_process: cellular response to organonitrogen compound	F55A8.2a.1, R13H8.1h
GO:0010243	0.00012298789603415274	2/12	biological_process: response to organonitrogen compound	F55A8.2a.1, R13H8.1h
GO:0019216	0.00012298789603415274	2/12	biological_process: regulation of lipid metabolic process	R12B2.5a.1, R13H8.1h
GO:0040014	0.00013496408717808206	5/378	biological_process: regulation of multicellular organism growth	R12B2.5a.1, R13H8.1h, F55A8.2a.1, Y106G6E.6.2, T19E7.2a.2
GO:0006950	0.0001387145778066839	6/620	biological_process: response to stress	R12B2.5a.1, R13H8.1h, F43C1.2b, C07F11.1, T19E7.2a.2, B0261.2a.2
GO:0045926	0.00014522855985186472	2/13	biological_process: negative regulation of growth	F55A8.2a.1, R13H8.1h
GO:0010941	0.00014856125489775908	3/75	biological_process: regulation of cell death	R13H8.1h, T19A5.2a, F38A6.3b
GO:0006357	0.00016059701159665688	4/204	biological_process: regulation of transcription by RNA polymerase II	R13H8.1h, R12B2.5a.1, T19E7.2a.2, W01G7.1
GO:0048638	0.0001639937735678753	5/394	biological_process: regulation of developmental growth	R12B2.5a.1, R13H8.1h, F55A8.2a.1, Y106G6E.6.2, T19E7.2a.2
GO:1901701	0.00022287211783579295	2/16	biological_process: cellular response to oxygen-containing compound	F55A8.2a.1, R13H8.1h
GO:0040024	0.00023076643668788746	3/87	biological_process: dauer larval development	K04G7.3a, R13H8.1h, B0261.2a.2
GO:0051093	0.0002631091962578787	4/232	biological_process: negative regulation of developmental process	F55A8.2a.1, H39E23.1d, R13H8.1h, F43C1.2b
GO:0051241	0.0003042773862588395	4/241	biological_process: negative regulation of multicellular organismal process	F55A8.2a.1, H39E23.1d, R13H8.1h, F43C1.2b
GO:1901699	0.00035170974055428366	2/20	biological_process: cellular response to nitrogen compound	F55A8.2a.1, R13H8.1h
GO:0043053	0.0003884089275148486	2/21	biological_process: dauer entry	R13H8.1h, B0261.2a.2
GO:0055115	0.0003884089275148486	2/21	biological_process: entry into diapause	R13H8.1h, B0261.2a.2
GO:1901698	0.0003884089275148486	2/21	biological_process: response to nitrogen compound	F55A8.2a.1, R13H8.1h
GO:0009893	0.00042423569547677856	4/263	biological_process: positive regulation of metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2, B0261.2a.2
GO:0000977	0.0004268949502622874	2/22	molecular_function: RNA polymerase II regulatory region sequence-specific DNA binding	R13H8.1h, T19E7.2a.2
GO:0045893	0.0004729004576630732	3/111	biological_process: positive regulation of transcription	R13H8.1h, R12B2.5a.1, T19E7.2a.2
GO:1902680	0.0004729004576630732	3/111	biological_process: positive regulation of RNA biosynthetic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2
GO:1903508	0.0004729004576630732	3/111	biological_process: positive regulation of nucleic acid-templated transcription	R13H8.1h, R12B2.5a.1, T19E7.2a.2
GO:0051254	0.0004982965964699399	3/113	biological_process: positive regulation of RNA metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2
GO:0048583	0.0005092913335825848	4/276	biological_process: regulation of response to stimulus	R13H8.1h, F55A8.2a.1, T19A5.2a, T19E7.2a.2
GO:0000976	0.0005530288285973636	2/25	molecular_function: transcription regulatory region sequence-specific DNA binding	R13H8.1h, T19E7.2a.2
GO:0001012	0.0005530288285973636	2/25	molecular_function: RNA polymerase II regulatory region DNA binding	R13H8.1h, T19E7.2a.2
GO:0007166	0.0005655832694445907	3/118	biological_process: cell surface receptor signaling pathway	R13H8.1h, F55A8.2a.1, C07F11.1
GO:0045935	0.0005655832694445907	3/118	biological_process: positive regulation of nucleobase-containing compound metabolic process	R13H8.1h, R12B2.5a.1, T19E7.2a.2
GO:0009605	0.0005980654354031306	4/288	biological_process: response to external stimulus	F38A6.3b, R13H8.1h, R12B2.5a.1, C07F11.1
