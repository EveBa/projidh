GO:0009408	7.653127884524355e-07	4/53	biological_process: response to heat	T19E7.2a.2, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0050793	1.7387898569018102e-06	8/691	biological_process: regulation of developmental process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, F55A8.2a.1, T19A5.2a, R13H8.1h, H39E23.1d
GO:0009266	2.2283034350079098e-06	4/69	biological_process: response to temperature stimulus	T19E7.2a.2, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0048519	2.322231689832789e-06	8/718	biological_process: negative regulation of biological process	F43C1.2b, K10B2.1, F55A8.2a.1, T19A5.2a, W01G7.1, R13H8.1h, F38A6.3b, H39E23.1d
GO:0051239	2.7697815830550617e-06	8/735	biological_process: regulation of multicellular organismal process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, F55A8.2a.1, T19A5.2a, R13H8.1h, H39E23.1d
GO:0043170	4.135716610856909e-06	13/2747	biological_process: macromolecule metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, M03C11.5.2, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0044260	4.272042929264716e-06	12/2248	biological_process: cellular macromolecule metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0005515	5.935312511107311e-06	10/1463	molecular_function: protein binding	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, ZK742.1a.1, T19A5.2a, W01G7.1, R13H8.1h, F38A6.3b, H39E23.1d
GO:0008340	6.230932381900071e-06	8/819	biological_process: determination of adult lifespan	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, F55A8.2a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0010259	6.230932381900071e-06	8/819	biological_process: multicellular organism aging	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, F55A8.2a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0007568	6.287898691692787e-06	8/820	biological_process: aging	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, F55A8.2a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0065007	1.047636626050408e-05	14/3572	biological_process: biological regulation	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, T19A5.2a, B0261.2a.2, C07F11.1, W01G7.1, R13H8.1h, F38A6.3b, H39E23.1d
GO:0034605	1.1255331743858473e-05	2/4	biological_process: cellular response to heat	T19E7.2a.2, R13H8.1h
GO:0043620	1.1255331743858473e-05	2/4	biological_process: regulation of DNA-templated transcription in response to stress	T19E7.2a.2, R13H8.1h
GO:0002119	1.33006844246375e-05	11/2019	biological_process: nematode larval development	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, ZK742.1a.1, B0261.2a.2, C07F11.1, R13H8.1h, H39E23.1d
GO:0002164	1.3432286346683248e-05	11/2021	biological_process: larval development	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, ZK742.1a.1, B0261.2a.2, C07F11.1, R13H8.1h, H39E23.1d
GO:0050789	1.439321336964758e-05	13/3051	biological_process: regulation of biological process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, F55A8.2a.1, T19A5.2a, B0261.2a.2, C07F11.1, W01G7.1, R13H8.1h, F38A6.3b, H39E23.1d
GO:0009791	1.4598339334432433e-05	11/2038	biological_process: post-embryonic development	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, ZK742.1a.1, B0261.2a.2, C07F11.1, R13H8.1h, H39E23.1d
GO:0006807	1.8244043536643146e-05	13/3113	biological_process: nitrogen compound metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, M03C11.5.2, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0006950	2.0099961845475336e-05	7/675	biological_process: response to stress	R12B2.5a.1, T19E7.2a.2, F43C1.2b, B0261.2a.2, C07F11.1, R13H8.1h, F38A6.3b
GO:0010557	2.2329648308035293e-05	4/123	biological_process: positive regulation of macromolecule biosynthetic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0009891	2.4554225554956706e-05	4/126	biological_process: positive regulation of biosynthetic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0031328	2.4554225554956706e-05	4/126	biological_process: positive regulation of cellular biosynthetic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0010628	2.5330728514871096e-05	4/127	biological_process: positive regulation of gene expression	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:1901700	3.204837273527241e-05	3/45	biological_process: response to oxygen-containing compound	F55A8.2a.1, T19E7.2a.2, R13H8.1h
GO:0042981	4.407402602904628e-05	3/50	biological_process: regulation of apoptotic process	T19A5.2a, R13H8.1h, F38A6.3b
GO:0005634	4.489931700737177e-05	9/1426	cellular_component: nucleus	R12B2.5a.1, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, ZK742.1a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0044238	5.383743450442453e-05	13/3415	biological_process: primary metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, M03C11.5.2, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0008134	5.558156583308715e-05	3/54	molecular_function: transcription factor binding	R12B2.5a.1, R13H8.1h, F38A6.3b
GO:0040015	6.725182182970247e-05	2/9	biological_process: negative regulation of multicellular organism growth	F55A8.2a.1, R13H8.1h
GO:0051173	6.732929929268083e-05	4/163	biological_process: positive regulation of nitrogen compound metabolic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0009628	6.895255254913264e-05	4/164	biological_process: response to abiotic stimulus	T19E7.2a.2, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0010604	7.228467448136957e-05	4/166	biological_process: positive regulation of macromolecule metabolic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0071704	7.372079431193305e-05	13/3509	biological_process: organic substance metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, M03C11.5.2, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0050794	7.876295423230713e-05	11/2420	biological_process: regulation of cellular process	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, F55A8.2a.1, T19A5.2a, B0261.2a.2, C07F11.1, W01G7.1, R13H8.1h, F38A6.3b
GO:0031325	8.112886599402413e-05	4/171	biological_process: positive regulation of cellular metabolic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0048522	8.181148848503554e-05	5/340	biological_process: positive regulation of cellular process	R12B2.5a.1, T19E7.2a.2, F43C1.2b, B0261.2a.2, R13H8.1h
GO:0008286	8.399491844608697e-05	2/10	biological_process: insulin receptor signaling pathway	F55A8.2a.1, R13H8.1h
GO:0032868	8.399491844608697e-05	2/10	biological_process: response to insulin	F55A8.2a.1, R13H8.1h
GO:0032869	8.399491844608697e-05	2/10	biological_process: cellular response to insulin stimulus	F55A8.2a.1, R13H8.1h
GO:0043434	8.399491844608697e-05	2/10	biological_process: response to peptide hormone	F55A8.2a.1, R13H8.1h
GO:0071375	8.399491844608697e-05	2/10	biological_process: cellular response to peptide hormone stimulus	F55A8.2a.1, R13H8.1h
GO:1901652	8.399491844608697e-05	2/10	biological_process: response to peptide	F55A8.2a.1, R13H8.1h
GO:1901653	8.399491844608697e-05	2/10	biological_process: cellular response to peptide	F55A8.2a.1, R13H8.1h
GO:0043067	8.828870069418595e-05	3/63	biological_process: regulation of programmed cell death	T19A5.2a, R13H8.1h, F38A6.3b
GO:0044237	0.00010182771605570685	12/3017	biological_process: cellular metabolic process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, T19A5.2a, B0261.2a.2, R13H8.1h, F38A6.3b, H39E23.1d
GO:0048640	0.00010257514738405587	2/11	biological_process: negative regulation of developmental growth	F55A8.2a.1, R13H8.1h
GO:0071417	0.00010257514738405587	2/11	biological_process: cellular response to organonitrogen compound	F55A8.2a.1, R13H8.1h
GO:0019216	0.00012298789603415274	2/12	biological_process: regulation of lipid metabolic process	R12B2.5a.1, R13H8.1h
GO:0040014	0.00013496408717808206	5/378	biological_process: regulation of multicellular organism growth	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F55A8.2a.1, R13H8.1h
GO:0045926	0.00014522855985186472	2/13	biological_process: negative regulation of growth	F55A8.2a.1, R13H8.1h
GO:0048638	0.0001639937735678753	5/394	biological_process: regulation of developmental growth	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F55A8.2a.1, R13H8.1h
GO:0010941	0.00016693609280824228	3/78	biological_process: regulation of cell death	T19A5.2a, R13H8.1h, F38A6.3b
GO:0006357	0.00017306376083779567	4/208	biological_process: regulation of transcription by RNA polymerase II	R12B2.5a.1, T19E7.2a.2, W01G7.1, R13H8.1h
GO:0040008	0.0002091320040165184	5/415	biological_process: regulation of growth	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F55A8.2a.1, R13H8.1h
GO:0040024	0.00023076643668788746	3/87	biological_process: dauer larval development	K04G7.3a, R13H8.1h, B0261.2a.2
GO:0043231	0.0002487309166690924	10/2224	cellular_component: intracellular membrane-bounded organelle	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, ZK742.1a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0051093	0.0002631091962578787	4/232	biological_process: negative regulation of developmental process	F55A8.2a.1, R13H8.1h, H39E23.1d, F43C1.2b
GO:0043227	0.00028452822288642534	10/2259	cellular_component: membrane-bounded organelle	R12B2.5a.1, T19E7.2a.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, ZK742.1a.1, B0261.2a.2, R13H8.1h, F38A6.3b
GO:0051241	0.0003042773862588395	4/241	biological_process: negative regulation of multicellular organismal process	F55A8.2a.1, R13H8.1h, H39E23.1d, F43C1.2b
GO:0010243	0.00035170974055428366	2/20	biological_process: response to organonitrogen compound	F55A8.2a.1, R13H8.1h
GO:0043053	0.0003884089275148486	2/21	biological_process: dauer entry	B0261.2a.2, R13H8.1h
GO:0055115	0.0003884089275148486	2/21	biological_process: entry into diapause	B0261.2a.2, R13H8.1h
GO:0032501	0.00040016496501540027	14/4769	biological_process: multicellular organismal process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, K10B2.1, K04G7.3a, F55A8.2a.1, ZK742.1a.1, T19A5.2a, B0261.2a.2, C07F11.1, R13H8.1h, F38A6.3b, H39E23.1d
GO:0000977	0.0004268949502622874	2/22	molecular_function: RNA polymerase II regulatory region sequence-specific DNA binding	T19E7.2a.2, R13H8.1h
GO:1901701	0.0004268949502622874	2/22	biological_process: cellular response to oxygen-containing compound	F55A8.2a.1, R13H8.1h
GO:0009893	0.0004366018630631672	4/265	biological_process: positive regulation of metabolic process	R12B2.5a.1, T19E7.2a.2, B0261.2a.2, R13H8.1h
GO:0045893	0.0004729004576630732	3/111	biological_process: positive regulation of transcription	R12B2.5a.1, T19E7.2a.2, R13H8.1h
GO:1902680	0.0004729004576630732	3/111	biological_process: positive regulation of RNA biosynthetic process	R12B2.5a.1, T19E7.2a.2, R13H8.1h
GO:1903508	0.0004729004576630732	3/111	biological_process: positive regulation of nucleic acid-templated transcription	R12B2.5a.1, T19E7.2a.2, R13H8.1h
GO:0051254	0.0004982965964699399	3/113	biological_process: positive regulation of RNA metabolic process	R12B2.5a.1, T19E7.2a.2, R13H8.1h
GO:0000976	0.0005530288285973636	2/25	molecular_function: transcription regulatory region sequence-specific DNA binding	T19E7.2a.2, R13H8.1h
GO:0001012	0.0005530288285973636	2/25	molecular_function: RNA polymerase II regulatory region DNA binding	T19E7.2a.2, R13H8.1h
GO:1901699	0.0005530288285973636	2/25	biological_process: cellular response to nitrogen compound	F55A8.2a.1, R13H8.1h
GO:0045935	0.0005655832694445907	3/118	biological_process: positive regulation of nucleobase-containing compound metabolic process	R12B2.5a.1, T19E7.2a.2, R13H8.1h
GO:0009605	0.0006218813307617173	4/291	biological_process: response to external stimulus	R12B2.5a.1, F38A6.3b, R13H8.1h, C07F11.1
GO:0048583	0.0006463677314072388	4/294	biological_process: regulation of response to stimulus	F55A8.2a.1, T19E7.2a.2, T19A5.2a, R13H8.1h
GO:0048518	0.0007306161933432787	6/842	biological_process: positive regulation of biological process	R12B2.5a.1, T19E7.2a.2, Y106G6E.6.2, F43C1.2b, B0261.2a.2, R13H8.1h
GO:0044212	0.0007985679548979701	2/30	molecular_function: transcription regulatory region DNA binding	T19E7.2a.2, R13H8.1h
