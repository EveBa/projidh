#!/usr/bin/env python
# Copyright 2018 BARRIOT Roland
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
from os.path import isfile
from scipy.stats import binom, hypergeom, chi2_contingency
import numpy as np

# SCRIPT PARAMETERS
# e.g. ./blastset.py --sets EcolA.biocyc.sets --query 'ALAS ARGS ASNS ASPS CYSS GLTX GLYQ GLYS HISS ILES'
parser = argparse.ArgumentParser(description='Search enriched terms/categories in the provided (gene) set')
parser.add_argument('-q', '--query', required=True, help='Query set.')
parser.add_argument('-t', '--sets', required=True, help='Target sets (categories).')
parser.add_argument('-a', '--alpha', required=False, type=float, default=0.05, help='Significance threshold.')
parser.add_argument('-c', '--adjust', required=False, action="store_true", help='Adjust for multiple testing (FDR).')
parser.add_argument('-m', '--measure', required=False, default='binomial', help='Dissimilarity index: binomial (default), hypergeometric, chi2 or coverage. Other method are NOT YET IMPLEMENTED')
parser.add_argument('-l', '--limit', required=False, type=int, default=0, help='Maximum number of results to report.')
param = parser.parse_args()

class ComparedSet(object):
	def __init__(self, id, name = '', common = 0, size = 0, pvalue = 1, elements = [], common_elements = []):
		self.id = id
		self.name = name
		self.common = common
		self.size = size
		self.pvalue = pvalue
		self.elements = elements
		self.common_elements = common_elements

# LOAD QUERY #paramètre -q il lit contenu contenu fichier et pour chaque ligne il découpe en mots et il les mets dans l'ensemble query
text = param.query
query = set()
if isfile(text):
	with open(text) as f:
		content = f.read()
		lines = content.split('\n')
		for l in lines:
			if l!='':
				query |= set(l.split())#ajout des ensembles dans query
else: # parse string
	query |= set(text.split())

# LOAD REFERENCE SETS #chargement des ensembles cibles en mémoire (dictionnaire où l'id du Go term ou pathway pointe vers la liste/ens de gene qui font partis de cet ensemble cible)
def load_sets(filename):
	sets = {}
	ids = set()#correspond à la population
	with open( filename ) as f:
		content = f.read()
		lines = content.split('\n')
		for l in lines:
			words = l.split('\t')
			if len(words) > 2 and not words[0].startswith('#'):
				id = words.pop(0)
				name = words.pop(0)
				words = set(words)
				sets[ id ] = { 'name': name, 'elements': words}#clé= id ens , name = nom ens, elements = liste des elts compris dedans
				ids |= words
	return [ sets, len( ids ) ]
(sets, population_size) = load_sets(param.sets)

# EVALUATE SETS
results = []
query_size = len(query)
for id in sets:
	elements = sets[ id ][ 'elements' ]
	common_elements = elements.intersection( query )
	if param.measure=='binomial': # binom.cdf(>=success, attempts, proba)
		# p_success = 384/2064, 152 attempts, 61 success
		#~ pval = binom.pmf(61, 152, 384.0/2064)
		pval = binom.cdf( query_size - len(common_elements), query_size, 1 - float(len(elements))/population_size)
	elif param.measure=='hypergeometric': # hypergeom.sf(common-1, population, target, query) = 1-p( X <= x-1 ) = p( X >= x )
		pval = hypergeom.sf(len(common_elements)-1, population_size, len(elements), query_size)
	elif param.measure=='chi2':
		TabCont=np.array([[len(common_elements),query_size-len(common_elements)],[len(elements)-len(common_elements),population_size-query_size-len(elements)+len(common_elements)]])
		chi2, pval, dof, ex = chi2_contingency(TabCont, correction=False)
		#chi2 avec scipy
		#observed : array_like
		#The contingency table. The table contains the observed frequencies (i.e. number of occurrences) in each category. In the two-dimensional case, the table is often described as an “R x C table”.
	elif param.measure=='coverage':
		pval=1-((len(common_elements)/query_size)*(len(common_elements)/len(elements)))
	else:
		print('sorry, %s not (yet) implemented' % ( param.measure ))
		exit(1)
	r = ComparedSet( id, sets[id]['name'], len(common_elements), len(elements), pval, elements, common_elements)
	results.append( r )

# PRINT SIGNIFICANT RESULTS
results.sort(key=lambda an_item: an_item.pvalue)#Tri des résultats par p-valeur croissante donc on va avoir les plus petites p-valeurs en premier
i=1
for r in results:
	# FDR
	if param.adjust and r.pvalue > param.alpha * i / len(results): break #première p-valeur avec bonferonni et ensuite plus permissif  et dès que la p-valeur n'est plus significative on s'arrête
	# limited output
	if param.limit > 0 and i>param.limit: break#on peut décider de ne pas voir tous les résultats mais que 3 par ex
	# alpha threshold
	elif r.pvalue > param.alpha: break
	# OUTPUT
	print("%s\t%s\t%s/%s\t%s\t%s" % ( r.id, r.pvalue, r.common, r.size, r.name, ', '.join(r.common_elements)))
	i+=1